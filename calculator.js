let calc = document.getElementById("Calc");
calc.addEventListener('click', (event) => {
    let firstArg = Number(document.getElementById("Arg1").value);
    let secondArg = Number(document.getElementById("Arg2").value);
    let operator = document.getElementById("Operator").value;

    let result;

    switch (operator) {
        // TODO: Implement all basic arithmetic operators
        case '+':
            result = firstArg + secondArg;
            break;

        case '-':
            result = firstArg - secondArg;
            break;

        default:
            console.error("Unknown operator")
    }

    // TODO: Update document with result
    console.log(result);
});
